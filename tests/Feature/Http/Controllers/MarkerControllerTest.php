<?php

namespace Tests\Feature\Http\Controllers;

use App\Marker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\MarkerController
 */
class MarkerControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $markers = factory(Marker::class, 3)->create();

        $response = $this->get(route('marker.index'));
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\MarkerController::class,
            'store',
            \App\Http\Requests\MarkerStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $marker = $this->faker->word;

        $response = $this->post(route('marker.store'), [
            'marker' => $marker,
        ]);

        $markers = Marker::query()
            ->where('marker', $marker)
            ->get();
        $this->assertCount(1, $markers);
        $marker = $markers->first();
    }


    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $marker = factory(Marker::class)->create();

        $response = $this->get(route('marker.show', $marker));
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\MarkerController::class,
            'update',
            \App\Http\Requests\MarkerUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $marker = factory(Marker::class)->create();
        $marker = $this->faker->word;

        $response = $this->put(route('marker.update', $marker), [
            'marker' => $marker,
        ]);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_responds_with()
    {
        $marker = factory(Marker::class)->create();

        $response = $this->delete(route('marker.destroy', $marker));

        $response->assertOk();

        $this->assertDeleted($marker);
    }
}
