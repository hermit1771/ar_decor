<?php

namespace Tests\Feature\Http\Controllers;

use App\PaymentHistory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\PaymentHistoryController
 */
class PaymentHistoryControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $paymentHistories = factory(PaymentHistory::class, 3)->create();

        $response = $this->get(route('payment-history.index'));
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PaymentHistoryController::class,
            'store',
            \App\Http\Requests\PaymentHistoryStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $paymentHistory = $this->faker->word;

        $response = $this->post(route('payment-history.store'), [
            'paymentHistory' => $paymentHistory,
        ]);

        $paymentHistories = PaymentHistory::query()
            ->where('paymentHistory', $paymentHistory)
            ->get();
        $this->assertCount(1, $paymentHistories);
        $paymentHistory = $paymentHistories->first();
    }


    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $paymentHistory = factory(PaymentHistory::class)->create();

        $response = $this->get(route('payment-history.show', $paymentHistory));
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PaymentHistoryController::class,
            'update',
            \App\Http\Requests\PaymentHistoryUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $paymentHistory = factory(PaymentHistory::class)->create();
        $paymentHistory = $this->faker->word;

        $response = $this->put(route('payment-history.update', $paymentHistory), [
            'paymentHistory' => $paymentHistory,
        ]);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_responds_with()
    {
        $paymentHistory = factory(PaymentHistory::class)->create();

        $response = $this->delete(route('payment-history.destroy', $paymentHistory));

        $response->assertOk();

        $this->assertDeleted($paymentHistory);
    }
}
