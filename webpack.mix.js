const mix = require('laravel-mix');
require('laravel-mix-alias');

const CompressionPlugin = require('compression-webpack-plugin');

if (mix.inProduction()) {
    mix.webpackConfig({
        plugins: [
            new CompressionPlugin({
                filename: '[path].br[query]',
                algorithm: 'brotliCompress',
                test: /\.(js|css|html|svg)$/,
                compressionOptions: {level: 11},
                minRatio: 1,
                deleteOriginalAssets: false,
            })
        ],
    });
}

mix.alias({
    '@': '/resources/js',
    '~': '/resources/sass',
    '@components': '/resources/js/components',
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


/*
mix.copyDirectory('resources/assets/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/js', 'public/js');
mix.copyDirectory('resources/assets/images', 'public/images');
*/



if (!mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'inline-source-map'
    })

    mix.js('resources/js/app.js', 'public/js')
        .sass('resources/sass/app.scss', 'public/css')
        .sourceMaps()
        .options({
            processCssUrls: false
        })
}
else {
    mix.js('resources/js/app.js', 'public/js')
        .sass('resources/sass/app.scss', 'public/css')
        .options({
            processCssUrls: false
        })
}


if (mix.inProduction()) {
    mix.minify('public/js/app.js')
    mix.minify('public/css/app.css')

}
mix.version();

mix.disableNotifications();
