@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Таблица маркеров</div>

                    <div class="card-body">
                        <markers-table></markers-table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
