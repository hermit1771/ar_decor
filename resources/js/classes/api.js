export default {

    loadARObjectList(self) {
        self.current_page_url = `/api/a-r-object/creator`;
        window
            .api
            .LoadList(self);
    },
    loadAchievementList(self) {
        self.current_page_url = '/api/achievements/creator';
        window
            .api
            .LoadList(self);
    },
    saveMarker(self) {
        let form = self.form
        form.creator_id = self.user_id ? self.user_id : form.creator_id

        window
            .axios
            .post('/api/marker', form)
            .then(resp => {
                self.$emit('submit')
            })
    },
    saveInstitution(self) {
        let form = self.form
        form.is_active = self.form.is_active === true ? 1 : 0;
        form.geo_coords = JSON.stringify(self.form.geo_coords)
        form.creator_id = self.user_id ? self.user_id : form.creator_id

        window
            .axios
            .post('/api/institution', form)
            .then(resp => {
                self.$emit('submit')
            })
    },
    duplicateMarker(self, id) {
        let url = `/api/marker/duplicate/${id}`

        window
            .api
            .duplicate(self, url)
            .then(reps => {
                window
                    .api
                    .loadMarkerList(self)
            })


    },
    duplicateInstitution(self, id) {
        let url = `/api/institution/duplicate/${id}`

        window
            .api
            .duplicate(self, url)
            .then(reps => {
                window
                    .api
                    .loadInstitutionList(self)
            })


    },
    loadInstitutionList(self) {
        self.current_page_url = `/api/institution/creator`;
        window
            .api
            .LoadList(self);

    },
    loadMarkerList(self) {
        self.current_page_url = '/api/marker/creator';
        window
            .api
            .LoadList(self);
    },
    prev(self) {
        self.current_page_url = self.prev_page_url
        window
            .api
            .LoadList(self);
    },
    next(self) {
        self.current_page_url = self.next_page_url
        window
            .api
            .LoadList(self);
    },
    LoadList(self) {
        self.preloader = true;

        window
            .axios
            .post(self.current_page_url, {
                user_id: self.user_id
            })
            .then(resp => {
                self.items = resp.data.data

                self.jsonstr = JSON.stringify(resp.data)

                self.total = resp.data.total
                self.first_page_url = resp.data.first_page_url
                self.next_page_url = resp.data.next_page_url
                self.prev_page_url = resp.data.prev_page_url

                self.preloader = false;


            })

    },
    async duplicate(self, url) {
        return await window
            .axios
            .get(url)
            .then(resp => {
                console.log(resp.data.message)
            })
    },
    async remove(self, url) {
        return await window
            .axios
            .get(url)
            .then(resp => {
                console.log(resp.data.message)
            })
    }


}
