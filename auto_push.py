
import subprocess

commit = input("your commit\n")
# login: SergeuG1
# password: <secret>
subprocess.call('git config --global credential.helper store')
subprocess.call('git add .')
subprocess.call(f'git commit -m "{commit}"')
subprocess.call('git remote add git_url_1 https://github.com/SergeuG1/ar_decor')
subprocess.call('git remote add git_url_2 https://gitlab.com/SergeuG1/ar_decor.git')
subprocess.call('git remote add git_url_3 https://hermit1771@bitbucket.org/hermit1771/ar_decor.git')

subprocess.call('git push -u git_url_1 master')
subprocess.call('git push -u git_url_2 master')
subprocess.call('git push git_url_3 master --force')
