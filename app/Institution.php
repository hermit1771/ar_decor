<?php

namespace App;

use App\Enums\InstitutionTypeEnum;
use App\Enums\ObjectTypeEnum;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Institution extends Model
{
    use CastsEnums, SoftDeletes;

    protected $enumCasts = [
        // 'attribute_name' => Enum::class
        'type' => InstitutionTypeEnum::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image',
        'type',
        'is_active',
        'position',
        'assigned_to_id',
        'geo_coords',
        'creator_id',
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'position' => 'integer',
        'is_active' => 'boolean',
        'geo_coords' => 'array'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    public $appends = [
        'is_worked'
    ];

    public $hidden = [
        'is_active','assigned_to_id'
    ];

    public function getIsWorkedAttribute()
    {
        return $this->is_active &&
            ($this->payment->current_money ?? 0) > ($this->payment->min_money_limit ?? 0);
    }

    public function markers()
    {
        return $this->hasMany(\App\Marker::class, 'institution_id', 'id');
    }

    public function history()
    {
        return $this->hasMany(\App\PaymentHistory::class, 'institution_id', 'id');
    }

    public static function getNearestInstitution($latitude, $longitude)
    {
        $dist = config('app.geo_institution_global_distance'); #дистанция 20 км

        $lon1 = $longitude - $dist / abs(cos(rad2deg($latitude)) * 111.0); # 1 градус широты = 111 км
        $lon2 = $longitude + $dist / abs(cos(rad2deg($latitude)) * 111.0);
        $lat1 = $latitude - ($dist / 111.0);
        $lat2 = $latitude + ($dist / 111.0);

        return Institution::whereBetween('geo_coords->latitude', [$lat1, $lat2])
            ->whereBetween('geo_coords->longitude', [$lon1, $lon2])
            ->get();


    }
}
