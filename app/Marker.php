<?php

namespace App;

use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Marker extends Model
{
    use CastsEnums, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sticker_image',
        'institution_id',
        'object_id',
        'image_set',
        'is_active',
        'title',
        'position',
        'description',
        'action_link',
        'action_triggers',
        'creator_id',
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'creator_id' => 'integer',
        'institution_id' => 'integer',
        'position' => 'integer',
        'image_set' => 'array',
        'action_triggers' => 'array',
        'is_active' => 'boolean',
        'object_id' => 'integer',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    protected $hidden = [
        'institution_id'
    ];


    public function aRObject()
    {
        return $this->hasOne(\App\ARObject::class, "id", "object_id");
    }

    public function institution()
    {
        return $this->belongsTo(\App\Institution::class);
    }

    public static function runActionTriggers($markerId)
    {
        $marker = Marker::find($markerId);


        if (is_null($marker))
            return;

        if (is_null($marker->action_triggers))
            return;

        $scaned = ScanedMarker::where("marker_id", $marker->id)
            ->where("user_id", Auth::user()->id)
            ->first();

        if (!is_null($scaned))
            return;

        ScanedMarker::create([
            "marker_id" => $marker->id,
            "user_id" => Auth::user()->id
        ]);

        foreach (json_decode($marker->action_triggers) as $trigger)
            event(new \App\Events\AchievementEvent(\App\Trigger::get($trigger->key), $trigger->value));

    }
}
