<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Achievement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'ach_image_url' => $this->ach_image_url,
            'trigger_type' => new Trigger($this->trigger),
            'trigger_value' => $this->trigger_value,
            'prize_description' => $this->prize_description,
            'prize_image_url' => $this->prize_image_url,
            'position' => $this->position,
            'is_active' => (boolean)$this->is_active,
         /*   'take_a_prize' => $this->pivot ? (boolean)$this->pivot->activated : false*/
        ];
    }
}
