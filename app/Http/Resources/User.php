<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'stats' => new StatCollection($this->stats),
            'achievements' => new AchievementCollection($this->achievements()->paginate(20)),
            'payment' => new Payment($this->payment)
        ];
    }
}
