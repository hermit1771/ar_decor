<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Payment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'comment' => $this->comment,
            'current_money' => $this->current_money,
            'min_money_limit' => $this->min_money_limit,
            'money_per_day' => $this->money_per_day,
        ];
    }
}
