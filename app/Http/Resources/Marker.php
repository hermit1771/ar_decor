<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Marker extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sticker_image' => $this->sticker_image,
            //'institution_id' => $this->institution_id,
            'image_set' => json_decode($this->image_set),
            'is_active' => $this->is_active,
            'title' => $this->title,
            'description' => $this->description,
            'action_link' => $this->action_link,
            'action_triggers' => json_decode($this->action_triggers),
            'position' => $this->position,
            'creator_id' => $this->creator_id,
            'a_r_object'=>new ARObject($this->aRObject)

        ];
    }
}
