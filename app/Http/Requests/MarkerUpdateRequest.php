<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarkerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sticker_image' => 'required|string|max:1000',
            'institution_id' => 'required|integer|exists:institutions,id',
            'image_set' => 'required|json',
            'is_active' => 'required',
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'action_link' => 'required|string',
            'action_triggers' => 'required|json',
            'position' => 'required|integer',
        ];
    }
}
