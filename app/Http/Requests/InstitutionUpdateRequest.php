<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstitutionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'image' => 'required|string|max:1000',
            'type' => 'required|integer',
            'is_active' => 'required',
            'position' => 'required|integer',
            'assigned_to_id' => 'nullable|integer',
            'geo_coords' => 'required|json',
        ];
    }
}
