<?php

namespace App\Http\Middleware;

use App\Marker;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ActionTriggerRunner
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            Marker::runActionTriggers($request->route('id'));
        }

        return $next($request);
    }
}
