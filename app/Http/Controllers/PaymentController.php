<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentStoreRequest;
use App\Http\Requests\PaymentUpdateRequest;
use App\Http\Resources\Payment as PaymentResource;
use App\Http\Resources\PaymentCollection;
use App\Http\Resources\PaymentHistory;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\PaymentCollection
     */
    public function index(Request $request)
    {
        $payments = Payment::simplePaginate(config('app.payment_per_page'));

        return new PaymentCollection($payments);
    }

    /**
     * @param \App\Http\Requests\PaymentStoreRequest $request
     * @return \App\Http\Resources\Payment
     */
    public function store(PaymentStoreRequest $request)
    {
        $payment = Payment::create($request->all());

        return new PaymentResource($payment);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Payment $payment
     * @return \App\Http\Resources\Payment
     */
    public function show(Request $request, Payment $payment)
    {
        return new PaymentResource($payment);
    }

    /**
     * @param \App\Http\Requests\PaymentUpdateRequest $request
     * @param \App\Payment $payment
     * @return \App\Http\Resources\Payment
     */
    public function update(PaymentUpdateRequest $request, Payment $payment)
    {
        $payment->update([]);

        return new PaymentResource($payment);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Payment $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Payment $payment)
    {
        $payment->delete();

        return response()->noContent(200);
    }

    public function payments(Request $request)
    {
        $user_id = Auth::user()->id;
        return new \App\Http\Resources\Payment(Payment::where("user_id", $user_id)->first());
    }

    public function history(Request $request)
    {
        $user_id = Auth::user()->id;

        return new \App\Http\Resources\PaymentHistoryCollection(\App\PaymentHistory::where("user_id", $user_id)->get());
    }
}
