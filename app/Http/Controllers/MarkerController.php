<?php

namespace App\Http\Controllers;

use App\Http\Requests\MarkerStoreRequest;
use App\Http\Requests\MarkerUpdateRequest;
use App\Http\Resources\Marker as MarkerResource;
use App\Http\Resources\MarkerCollection;
use App\Marker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MarkerController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\MarkerCollection
     */
    public function all(Request $request)
    {
        $markers = Marker::orderBy('position', 'DESC')->get();

        $deleted_markers = Marker::onlyTrashed()->get();

        return response()
            ->json([
                'items' => MarkerCollection($markers),
                'deleted_items' => MarkerCollection($deleted_markers),
            ], 200);

//        return new MarkerCollection($markers);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\MarkerCollection
     */
    public function index(Request $request)
    {
        $markers = Marker::orderBy('position', 'DESC')
            ->simplePaginate(config('app.marker_per_page'));

        return new MarkerCollection($markers);
    }

    /**
     * @param \App\Http\Requests\MarkerStoreRequest $request
     * @return \App\Http\Resources\Marker
     */
    public function store(MarkerStoreRequest $request)
    {
        $marker = Marker::create($request->all());

        return new MarkerResource($marker);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Marker $marker
     * @return \App\Http\Resources\Marker
     */
    public function show(Request $request, Marker $marker)
    {
        return new MarkerResource($marker);
    }

    /**
     * @param \App\Http\Requests\MarkerUpdateRequest $request
     * @param \App\Marker $marker
     * @return \App\Http\Resources\Marker
     */
    public function update(MarkerUpdateRequest $request, Marker $marker)
    {
        $marker->update([]);

        return new MarkerResource($marker);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Marker $marker
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marker = Marker::find($id);
        $marker->delete();

        return response()->noContent(200);
    }

    public function restore($id)
    {
        $marker = Marker::onlyTrashed()->where('id', $id)->restore();

        return response()
            ->json([
                "message" => "Маркер восстановлен",
                "status" => 200,
            ]);
    }

    public function save(Request $request, $id)
    {
        $param = $request->get("param");
        $value = $request->get("value");

        $marker = Marker::find($id);
        $marker[$param] = $value;
        $marker->save();

        return response()
            ->json([
                "message" => "Изменения сохранены",
            ], 200);
    }

    public function selfCreatedList(Request $request)
    {
        $userId = $request->get("user_id");

        return response()
            ->json(Marker::where("creator_id", $userId)->orderBy("id", "desc")->paginate(env("APP_PAGINATE_COUNT")));
    }
}
