<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentHistoryStoreRequest;
use App\Http\Requests\PaymentHistoryUpdateRequest;
use App\Http\Resources\PaymentHistory as PaymentHistoryResource;
use App\Http\Resources\PaymentHistoryCollection;
use App\PaymentHistory;
use Illuminate\Http\Request;

class PaymentHistoryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\PaymentHistoryCollection
     */
    public function index(Request $request)
    {
        $paymentHistories = PaymentHistory::simplePaginate(config('app.payment_history_per_page'));

        return new PaymentHistoryCollection($paymentHistories);
    }

    /**
     * @param \App\Http\Requests\PaymentHistoryStoreRequest $request
     * @return \App\Http\Resources\PaymentHistory
     */
    public function store(PaymentHistoryStoreRequest $request)
    {
        $paymentHistory = PaymentHistory::create($request->all());

        return new PaymentHistoryResource($paymentHistory);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\PaymentHistory $paymentHistory
     * @return \App\Http\Resources\PaymentHistory
     */
    public function show(Request $request, PaymentHistory $paymentHistory)
    {
        return new PaymentHistoryResource($paymentHistory);
    }

    /**
     * @param \App\Http\Requests\PaymentHistoryUpdateRequest $request
     * @param \App\PaymentHistory $paymentHistory
     * @return \App\Http\Resources\PaymentHistory
     */
    public function update(PaymentHistoryUpdateRequest $request, PaymentHistory $paymentHistory)
    {
        $paymentHistory->update([]);

        return new PaymentHistoryResource($paymentHistory);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\PaymentHistory $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PaymentHistory $paymentHistory)
    {
        $paymentHistory->delete();

        return response()->noContent(200);
    }
}
