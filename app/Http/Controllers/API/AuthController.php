<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Payment;
use App\Http\Resources\UserCollection;
use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    private $client;

    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', env("APP_CLIENT_ID"))->first();
    }

    public function authenticateByPhone(Request $request)
    {

        $valid = validator($request->only('phone', 'password'), [
            'phone' => 'required|string|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $proxy = Request::create(
            'oauth/token',
            'POST',
            [
                'grant_type' => 'password',
                'username' => $request->phone . "@" . env("APP_MAIL_DOMAIN"),
                'password' => $request->password,
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => ''
            ]
        );

        return app()->handle($proxy);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function authenticate(Request $request)
    {

        $valid = validator($request->only('email', 'password'), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $proxy = Request::create(
            'oauth/token',
            'POST',
            [
                'grant_type' => 'password',
                'username' => $request->email,
                'password' => $request->password,
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => ''
            ]
        );

        return app()->handle($proxy);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function refreshToken(Request $request)
    {
        $proxy = Request::create(
            'oauth/token',
            'POST',
            [
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->refresh_token,
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => ''
            ]
        );

        return app()->handle($proxy);
    }

    function signup(Request $request)
    {
        /**
         * Get a validator for an incoming registration request.
         *
         * @param array $request
         * @return \Illuminate\Contracts\Validation\Validator
         */
        $valid = validator($request->only('email', 'name', 'password'), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $data = request()->only('email', 'name', 'password');

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $baseUser = Role::where('slug', 'base-user')->first();

        $user->roles()->attach($baseUser);
        $user->permissions()->attach([
            Permission::where('slug', 'create-institution')->first()->id,
            Permission::where('slug', 'create-marker')->first()->id,
        ]);

        $token = Request::create(
            'oauth/token',
            'POST',
            [
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'username' => $data["email"],
                'password' => $data["password"],
                'scope' => '*'
            ]
        );
        return app()->handle($token);
    }


    function signupByPhone(Request $request)
    {
        /**
         * Get a validator for an incoming registration request.
         *
         * @param array $request
         * @return \Illuminate\Contracts\Validation\Validator
         */
        $valid = validator($request->only('phone'), [
            'phone' => [
                'required',
                'string',
                'max:255',
                function ($attribute, $value, $fail) {
                    $user = User::where("email", "$value@" . env("APP_MAIL_DOMAIN"))->first();
                    if (!is_null($user)) {
                        $fail($attribute . ' is invalid.');
                    }
                },
            ],

        ]);

        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return response()->json($jsonError);
        }

        $data = request()->only('phone');

        $userEmail = $data['phone'] . "@" . env("APP_MAIL_DOMAIN");
        $userPassword = rand(100000, 999999);
        $user = User::create([
            'name' => '',
            'email' => $userEmail,
            'password' => bcrypt($userPassword),
        ]);

        $baseUser = Role::where('slug', 'base-user')->first();

        $user->roles()->attach($baseUser);
        $user->permissions()->attach([
            Permission::where('slug', 'create-institution')->first()->id,
            Permission::where('slug', 'create-marker')->first()->id,
        ]);

        $token = Request::create(
            'oauth/token',
            'POST',
            [
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'username' => $userEmail,
                'password' => $userPassword,
                'scope' => '*'
            ]
        );
        return app()->handle($token);
    }


    public function getMe(Request $request)
    {
        $user = new \App\Http\Resources\User(auth()->guard('api')->user());

        if (is_null($user->payment))
            \App\Payment::create([
                'comment' => '',
                'current_money' => 100,
                'min_money_limit' => 0,
                'money_per_day' => 10,
                'user_id' => $user->id
            ]);


        return response()->json(new \App\Http\Resources\User(User::find($user->id)));
    }
}
