<?php

namespace App;

use App\Enums\TriggerAchievementEnum;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $fillable = [
        'title',
        'description',
        'ach_image_url',
        'trigger_type',
        'trigger_value',
        'prize_description',
        'prize_image_url',
        'position',
        'is_active',
        'creator_id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class,'user_achievement','achievement_id','user_id')
            ->withTimestamps();
    }

    public function trigger()
    {
        return $this->hasOne(Trigger::class, 'id', 'trigger_type');
    }


}
