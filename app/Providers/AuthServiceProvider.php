<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(now()->addDays(15));

        Passport::refreshTokensExpireIn(now()->addDays(30));

        Passport::personalAccessTokensExpireIn(now()->addMonths(6));

        Passport::tokensCan([
            'institution-show' => 'Просмотр доступных помещений',
            'institution-create' => 'Добавление нового помещения',
            'institution-update' => 'Обновление помещения',
            'institution-delete' => 'Удаление помещения',

            'arobject-show' => 'Просмотр доступных объектов',
            'arobject-create' => 'Добавление нового объекта',
            'arobject-update' => 'Обновление объекта',
            'arobject-delete' => 'Удаление объекта',

            'marker-show' => 'Просмотр доступных маркеров',
            'marker-create' => 'Добавление нового маркера',
            'marker-update' => 'Обновление маркера',
            'marker-delete' => 'Удаление маркера',


        ]);

        Passport::setDefaultScope([
            'institution-show',
            'marker-show',
            'arobject-show',
        ]);
    }
}
