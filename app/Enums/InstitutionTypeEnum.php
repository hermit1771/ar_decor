<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class InstitutionTypeEnum extends Enum
{
    const Building =   0;
    const Floor =   1;
    const Room = 2;
    const OpenAir = 3;
}
