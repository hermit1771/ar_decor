<?php

namespace App;

use App\Enums\TriggerAchievementEnum;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{

    protected $fillable = [
        'stat_type',
        'stat_value',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function trigger()
    {
        return $this->hasOne(Trigger::class, 'id', 'stat_type');
    }

}
