<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Integer;
use phpDocumentor\Reflection\Types\String_;

class Trigger extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'title',
        'description',
        'creator_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'creator_id' => 'integer',
    ];

    public static function has($key)
    {
        return Trigger::get($key) !== null;
    }

    public static function get($key)
    {
        return (is_numeric($key) ?
                Trigger::where("id", $key)->first() :
                Trigger::where("key", $key)->first()
            ) ?? null;
    }
}
