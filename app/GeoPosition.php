<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeoPosition extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'prev_image_set',
        'latitude',
        'longitude',
        'object_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'latitude' => 'double',
        'longitude' => 'double',
        'object_id' => 'integer',
        'prev_image_set' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    protected $hidden = [
        'deleted_at'
    ];


    public function aRObject()
    {
        return $this->hasOne(\App\ARObject::class,'id','object_id');
    }

    public static function getNearestPoints($latitude, $longitude, $dist = 0.2)
    {

        $lon1 = $longitude - $dist / abs(cos(rad2deg($latitude)) * 111.0); # 1 градус широты = 111 км
        $lon2 = $longitude + $dist / abs(cos(rad2deg($latitude)) * 111.0);
        $lat1 = $latitude - ($dist / 111.0);
        $lat2 = $latitude + ($dist / 111.0);

        return GeoPosition::whereBetween('latitude', [$lat1, $lat2])
            ->whereBetween('longitude', [$lon1, $lon2])
            ->simplePaginate(config('app.geo_position_per_page'));


    }
}
