<?php

namespace App;

use App\Enums\ObjectTypeEnum;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ARObject extends Model
{
    use CastsEnums, SoftDeletes;

    protected $enumCasts = [
        // 'attribute_name' => Enum::class
        'object_type' => ObjectTypeEnum::class,
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'object_type',
        'object_path',
        'object_settings',
        'transform',
        'creator_id',
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',

        'transform' => 'array',
        'object_type' => 'integer',
        'object_path' => 'array',
        'object_settings' => 'array'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    protected $hidden = [
        'id'
    ];

    public function marker()
    {
        return $this->hasOne(\App\Marker::class,'object_id','id');
    }




}
