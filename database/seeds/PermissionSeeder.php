<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();
        $manageUser = new Permission();
        $manageUser->name = 'Manage users';
        $manageUser->slug = 'manage-users';
        $manageUser->save();
        $createTasks = new Permission();
        $createTasks->name = 'Create Tasks';
        $createTasks->slug = 'create-tasks';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'Create Institution';
        $createTasks->slug = 'create-institution';
        $createTasks->save();
        $createTasks = new Permission();
        $createTasks->name = 'Create Marker';
        $createTasks->slug = 'create-marker';
        $createTasks->save();
    }
}
