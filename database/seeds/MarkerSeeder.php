<?php

use App\Marker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class MarkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Marker::truncate();

        $last_id = \App\Institution::latest()->first()->id??1;

        Marker::create([
            'sticker_image' => '/test_stickers/1.jpg',
            'institution_id' => $last_id,
            'object_id' => 1,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/1.jpg'
                ],
                [
                    "url"=>'/test_markers/1_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 1",
            'description' => "Описание 1",
            'action_link' => "https://pro-ar.ru/api/actions/1/",
            'action_triggers' => json_encode([
                [
                    "key"=>1,
                    "value"=>10
                ],
                [
                    "key"=>2,
                    "value"=>10
                ],
                [
                    "key"=>3,
                    "value"=>10
                ],
            ]),
            'position' => 1,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/2.jpg',
            'institution_id' => $last_id,
            'object_id' => 2,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/2.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 2",
            'description' => "Описание 2",
            'action_link' => "https://pro-ar.ru/api/actions/2/",
            'position' => 2,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/3.jpg',
            'institution_id' => $last_id,
            'object_id' => 3,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/3.jpg'
                ],
                [
                    "url"=>'/test_markers/3_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 3",
            'description' => "Описание 3",
            'action_link' => "https://pro-ar.ru/api/actions/3/",
            'position' => 3,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/4.jpg',
            'institution_id' => $last_id,
            'object_id' => 4,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/4.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 4",
            'description' => "Описание 4",
            'action_link' => "https://pro-ar.ru/api/actions/4/",
            'position' => 4,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/5.jpg',
            'institution_id' => $last_id,
            'object_id' => 5,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/5.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 5",
            'description' => "Описание 5",
            'action_link' => "https://pro-ar.ru/api/actions/5/",
            'position' => 5,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/6.jpg',
            'institution_id' => $last_id,
            'object_id' => 6,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/6.jpg'
                ],

            ]),
            'is_active' => true,
            'title' => "Маркер 6",
            'description' => "Описание 6",
            'action_link' => "https://pro-ar.ru/api/actions/6/",
            'position' => 6,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/7.jpg',
            'institution_id' => $last_id,
            'object_id' => 7,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/7.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 7",
            'description' => "Описание 7",
            'action_link' => "https://pro-ar.ru/api/actions/7/",
            'position' => 7,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/8.jpg',
            'institution_id' => $last_id,
            'object_id' => 8,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/8.jpg'
                ],
                [
                    "url"=>'/test_markers/8_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 8",
            'description' => "Описание 8",
            'action_link' => "https://pro-ar.ru/api/actions/8/",
            'position' => 8,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/9.jpg',
            'institution_id' => $last_id,
            'object_id' => 9,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/9.jpg'
                ],
                [
                    "url"=>'/test_markers/9_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 9",
            'description' => "Описание 9",
            'action_link' => "https://pro-ar.ru/api/actions/9/",
            'position' => 9,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/10.jpg',
            'institution_id' => $last_id,
            'object_id' => 10,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/10.jpg'
                ],
                [
                    "url"=>'/test_markers/10_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 10",
            'description' => "Описание 10",
            'action_link' => "https://pro-ar.ru/api/actions/10/",
            'position' => 10,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/11.jpg',
            'institution_id' => $last_id,
            'object_id' => 11,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/11.jpg'
                ],
                [
                    "url"=>'/test_markers/11_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 11",
            'description' => "Описание 11",
            'action_link' => "https://pro-ar.ru/api/actions/11/",
            'position' => 11,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/12.jpg',
            'institution_id' => $last_id,
            'object_id' => 12,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/12.jpg'
                ],
                [
                    "url"=>'/test_markers/12_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 12",
            'description' => "Описание 12",
            'action_link' => "https://pro-ar.ru/api/actions/12/",
            'position' => 12,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/13.jpg',
            'institution_id' => $last_id,
            'object_id' => 13,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/13.jpg'
                ],
                [
                    "url"=>'/test_markers/13_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 13",
            'description' => "Описание 13",
            'action_link' => "https://pro-ar.ru/api/actions/13/",
            'position' => 13,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/14.jpg',
            'institution_id' => $last_id,
            'object_id' => 14,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/14.jpg'
                ],
                [
                    "url"=>'/test_markers/14_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 14",
            'description' => "Описание 14",
            'action_link' => "https://pro-ar.ru/api/actions/14/",
            'position' => 14,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/15.jpg',
            'institution_id' => $last_id,
            'object_id' => 15,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/15.jpg'
                ],
                [
                    "url"=>'/test_markers/15_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 15",
            'description' => "Описание 15",
            'action_link' => "https://pro-ar.ru/api/actions/15/",
            'position' => 15,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/16.jpg',
            'institution_id' => $last_id,
            'object_id' => 16,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/16.jpg'
                ],
                [
                    "url"=>'/test_markers/16_1.jpg'
                ]
            ]),
            'is_active' => true,
            'title' => "Маркер 16",
            'description' => "Описание 16",
            'action_link' => "https://pro-ar.ru/api/actions/16/",
            'position' => 16,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/17.jpg',
            'institution_id' => $last_id,
            'object_id' => 17,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/17.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 17",
            'description' => "Описание 17",
            'action_link' => "https://pro-ar.ru/api/actions/17/",
            'position' => 17,
        ]);


        Marker::create([
            'sticker_image' => '/test_stickers/18.jpg',
            'institution_id' => $last_id,
            'object_id' => 18,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/18.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 18",
            'description' => "Описание 18",
            'action_link' => "https://pro-ar.ru/api/actions/18/",
            'position' => 18,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/19.jpg',
            'institution_id' => $last_id,
            'object_id' => 19,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/19.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 18",
            'description' => "Описание 19",
            'action_link' => "https://pro-ar.ru/api/actions/19/",
            'position' => 19,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/20.jpg',
            'institution_id' => $last_id,
            'object_id' => 20,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/20.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 20",
            'description' => "Описание 20",
            'action_link' => "https://pro-ar.ru/api/actions/20/",
            'position' => 20,
        ]);

        Marker::create([
            'sticker_image' => '/test_stickers/21.jpg',
            'institution_id' => $last_id,
            'object_id' => 21,
            'image_set' => json_encode([
                [
                    "url"=>'/test_markers/21.jpg'
                ],
            ]),
            'is_active' => true,
            'title' => "Маркер 21",
            'description' => "Описание 21",
            'action_link' => "https://pro-ar.ru/api/actions/21/",
            'position' => 21,
        ]);


    }
}
