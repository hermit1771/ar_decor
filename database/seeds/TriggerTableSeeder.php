<?php

use Illuminate\Database\Seeder;

class TriggerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Trigger::truncate();

        \App\Trigger::create([
           'key'=>'SubscribeOnGroup',
           'title'=>''
        ]);

        \App\Trigger::create([
            'key'=>'OpenLocationCount',
            'title'=>''
        ]);

        \App\Trigger::create([
            'key'=>'ClickObjectCount',
            'title'=>''
        ]);

        \App\Trigger::create([
            'key'=>'Level',
            'title'=>''
        ]);

        \App\Trigger::create([
            'key'=>'Experience',
            'title'=>''
        ]);

        \App\Trigger::create([
            'key'=>'WaiterCallsCount',
            'title'=>'',
            'description'=>"Число вызовов официанта"
        ]);


    }
}
