<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $this->call(MarkerSeeder::class);
        //$this->call(AchievementsTableSeeder::class);
       // $this->call(TriggerTableSeeder::class);
       /* DB::table("users_roles")->truncate();
        DB::table("users_permissions")->truncate();


        $this->call(UserSeeder::class);*/

   /*     $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
/**/
       /* $this->call(InstitutionSeeder::class);

        $this->call(ARObjectSeeder::class);
        $this->call(MarkerSeeder::class);
        $this->call(GeoPositionSeeder::class);*/
        Schema::enableForeignKeyConstraints();

    }
}
