<?php

use App\Http\Resources\ARObject;
use Illuminate\Database\Seeder;

class GeoPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\GeoPosition::truncate();

        \App\GeoPosition::create([
            'title' => 'Test point 1',
            'description' => 'test decription 1',
            'latitude' => 48.0057877,
            'longitude' => 37.7942312,
            'object_id' => 1
        ]);
    }
}
