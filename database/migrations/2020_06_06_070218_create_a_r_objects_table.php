<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateARObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_r_objects', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100)->default('');
            $table->longText('description');

            $table->json('object_settings')->nullable();
            $table->json('transform');
            $table->tinyInteger('object_type')->default(0);
            $table->json('object_path')->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_r_objects');
    }
}
