<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->string('description',1000)->default('');
            $table->string('ach_image_url',1000)->nullable(false);
            $table->integer('trigger_type')->nullable(false);
            $table->integer('trigger_value')->default(0);
            $table->string('prize_description',1000)->nullable(false);
            $table->string('prize_image_url',1000)->nullable(false);
            $table->boolean('is_active')->default(true);
            $table->integer('position')->default(0);
            $table->unsignedInteger('creator_id')->nullable();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievements');
    }
}
