<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('markers', function (Blueprint $table) {
            $table->id();
            $table->string('sticker_image', 1000)->default('');

            $table->unsignedBigInteger('institution_id')->nullable();
            $table->foreign('institution_id')->references('id')->on('institutions');

            $table->unsignedBigInteger('object_id')->nullable();
            $table->foreign('object_id')->references('id')->on('a_r_objects');


            $table->json('image_set');
            $table->boolean('is_active')->default(true);
            $table->string('title', 100)->default('');
            $table->longText('description');
            $table->text('action_link');
            $table->json('action_triggers')->nullable();
            $table->integer('position')->default(0);
            $table->unsignedInteger('creator_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markers');
    }
}
