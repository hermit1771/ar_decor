<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeoPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('geo_positions', function (Blueprint $table) {
            $table->id();
            $table->string('title', 200)->default('');
            $table->string('description', 1000)->default('');
            $table->json('prev_image_set');
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);

            $table->unsignedBigInteger('object_id')->nullable();
            $table->foreign('object_id')->references('id')->on('a_r_objects');

            $table->softDeletes();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_positions');
    }
}
