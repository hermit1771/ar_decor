<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100)->default('');
            $table->longText('description');
            $table->string('image', 1000)->default('');
            $table->tinyInteger('type')->default('0');
            $table->boolean('is_active')->default(true);
            $table->integer('position')->default(0);
            $table->json('geo_coords')->nullable();
            $table->unsignedInteger('assigned_to_id')->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
