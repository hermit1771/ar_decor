<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScanedMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('scaned_markers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("marker_id");
            $table->unsignedInteger("user_id");

            if (env("DB_CONNECTION")=='mysql') {
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('marker_id')->references('id')->on('markers');
            }
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scaned_markers');
    }
}
