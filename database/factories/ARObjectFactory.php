<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ARObject;
use Faker\Generator as Faker;

$factory->define(ARObject::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(4),
        'description' => $faker->text,
        'marker_id' => factory(\App\Marker::class),
        'is_geo' => false,
        'geo_coords' => json_encode([
            "latitude" => 0.0,
            "longitude" => 0.0,
        ]),
        'transform' => json_encode([
            "position" => [
                "x" => 0.0,
                "y" => 0.0,
                "z" => 0.0,
            ],
            "rotation" => [
                "x" => 0.0,
                "y" => 0.0,
                "z" => 0.0,
            ],
            "scale" => [
                "x" => 0.0,
                "y" => 0.0,
                "z" => 0.0,
            ]
        ]),
        'object_type' => $faker->randomElement(\App\Enums\ObjectTypeEnum::toArray()),
        'object_path' => json_encode([
            "name" => "testObject",
            "url" => $faker->url
        ]),
        'object_settings'=>json_encode([
            "color" => "red",
        ]),

    ];
});
