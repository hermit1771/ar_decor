<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'comment' => $faker->text,
        'current_money' => $faker->numberBetween(0,1000),
        'min_money_limit' => $faker->numberBetween(0,1000),
        'money_per_day' => $faker->numberBetween(0,1000),
    ];
});
