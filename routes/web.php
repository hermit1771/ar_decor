<?php

//use App\Http\Controllers\ARObjectController;
//use App\Http\Controllers\InstitutionController;
//use App\Http\Controllers\MarkerController;
//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Controllers\ARObjectController;
use App\Http\Controllers\InstitutionController;
use App\Http\Controllers\MarkerController;
use Illuminate\Support\Facades\Storage;

Auth::routes();


Route::view('/', 'welcome');

Route::view('/map', 'map');

Route::view('/home','cabinet')->middleware(['auth']);
Route::get('/example/{id}',function ($id){
    return view('form-example',compact('id'));
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::view('/', 'home');
    Route::view('/institutions', 'admin.institutions-table');
    Route::get('/institution/all', 'InstitutionController@all');
    Route::post('/institution/restore/{id}', 'InstitutionController@restore');
    Route::put('/institution/save/{id}', 'InstitutionController@save');
    Route::view('/markers', 'admin.markers-table');
    Route::get('/marker/all', 'MarkerController@all');
    Route::post('/marker/restore/{id}', 'MarkerController@restore');
    Route::put('/marker/save/{id}', 'MarkerController@save');
    Route::view('/arobjects', 'admin.ar-objects-table');
    Route::get('/arobject/all', 'ARObjectController@all');
    Route::post('/arobject/restore/{id}', 'ARObjectController@restore');
    Route::put('/arobject/save/{id}', 'ARObjectController@save');

    Route::resource('institution', InstitutionController::class);
    Route::resource('marker', MarkerController::class);
    Route::resource('arobject', ARObjectController::class);
});

